import interactor from "../../Scenes/Login/LoginInteractor";
import presenter from "../../Scenes/Login/LoginPresenter";

import LoginMockStore from "../../Scenes/Login/LoginMockStore";

class LoginInteractorTest {
  constructor() {
    this.interactor = { ...interactor };
    this.interactor.store(new LoginMockStore());
    this.interactor.presenter(new LoginPresenterSpy());
  }
}

class LoginPresenterSpy {
  presentLoginResult = (result) => {};
  presentLoadingView = (show) => {};
}

let sut = new LoginInteractorTest();

describe("LoginInteractor", () => {
  beforeEach(() => {});

  afterEach(() => {});

  describe("login", () => {
    it("login() with sucess should ask presenter to present presentDoLogin()", () => {
      sut.interactor.login(1, 2);
    });
  });
});

import { viewController } from "./LoginViewController";

class LoginPresenter {
  presentLoginResult = (result) => {
    let viewModel = {
      result: result,
      message: result
        ? "🦄   Login success"
        : "🎃   Incorrect username or password",
    };
    console.log(viewController);
    viewController.displayDoLogin(viewModel);
  };

  presentLoadingView = (show) => {
    viewController.displayLoading(show);
  };
}

let proxy = new LoginPresenter();

export default {
  presentDoLogin: (result) => proxy.presentLoginResult(result),
  presentLoading: (show) => proxy.presentLoadingView(show),
};

let proxy = null;
let store = null;

function LoginWorkerProxy(repository) {
  store = repository;

  this.loginVerification = (request, callback) => {
    console.log(store, request);
    store.loginVerification(request, (response) => {
      console.log(response);
      callback(response);
    });
  };
}

function LoginWorker(store) {
  proxy = new LoginWorkerProxy(store);
}

LoginWorker.prototype = {
  verifyLogin: (params, callback) => {
    let request = { username: params.username, password: params.password };
    proxy.loginVerification(request, callback);
  },
};

export default LoginWorker;

import verifyLoginData from "../../Spec/GET_v1-login-verification.json";
class LoginMockStore {
  loginVerification = (params, callback) => {
    let response = verifyLoginData[0];
    console.log(response);
    callback(response);
  };
}

export default LoginMockStore;

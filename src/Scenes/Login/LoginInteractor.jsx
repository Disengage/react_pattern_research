import presenter from "./LoginPresenter";
import Worker from "./LoginWorker";

import LoginRestStore from "./LoginRestStore";
import LoginMockStore from "./LoginMockStore";

class LoginInterator {
  worker = null;
  presenter = null;

  constructor(store) {
    this.worker = new Worker(store);
    this.presenter = presenter;
  }

  doLogin = (username, password) => {
    console.log(presenter);
    presenter.presentLoading(true);
    this.loginValidation(username, password, (result) => {
      console.log(result);
      presenter.presentLoading(false);
      presenter.presentDoLogin(result.data != null);
    });
  };

  loginValidation = (username, password, callback) => {
    console.log(this.worker);
    if (username !== "disengage") {
      callback(false);
      return;
    }
    let params = { username: username, password: password };
    this.worker.verifyLogin(params, (response) => callback(response));
  };
}

var proxy = new LoginInterator(new LoginRestStore());

export default {
  store: (store) => {
    proxy.worker = new Worker(store);
  },
  presenter: (presenter) => {
    proxy.presenter = presenter;
  },
  login: (username, password) => proxy.doLogin(username, password),
};

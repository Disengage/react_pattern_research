import React, { useEffect, useRef, useState } from "react";

import interactor from "./LoginInteractor";

import styles from "./Login.module.css";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Loader from "react-loader-advanced";
//------------------------------------------------------

export const viewController = {
  displayDoLogin: null,
  displayLoading: null,
};

//------------------------------------------------------

function LoginViewController() {
  var username = useRef();
  var password = useRef();

  const [inlineView, setInlineMessage] = useState(null);
  const [showLoading, setShowLoading] = useState(false);

  useEffect(() => {
    viewController.displayDoLogin = displayDoLogin;
    viewController.displayLoading = displayLoading;
  }, []);

  const displayLoading = (viewModel) => {
    setShowLoading(viewModel === true);
  };

  const makeToast = (success, message) => {
    let options = {
      position: "top-right",
      autoClose: 2500,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: false,
      progress: undefined,
    };
    if (success) {
      return toast.success(message, options);
    } else {
      return toast(message, options);
    }
  };

  const displayDoLogin = (viewModel) => {
    console.log(viewModel);
    makeToast(viewModel.result === true, viewModel.message);
  };

  const onClickBtnSubmit = () => {
    console.log(interactor);
    interactor.login(username.current.value, password.current.value);
  };

  const onClickBtnReset = () => {
    setInlineMessage(null);
  };

  return (
    <Loader show={showLoading} message={"Loading ..."}>
      <div className={styles.container}>
        <ToastContainer
          position="top-right"
          autoClose={2500}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable={false}
          pauseOnHover
        />

        <ul>
          <li>
            <ul>
              <li>Username:</li>
              <li>
                <input
                  type="text"
                  id="username"
                  name="username"
                  ref={username}
                />
              </li>
            </ul>
          </li>
          <li>
            <ul>
              <li>Password:</li>
              <li>
                <input
                  type="password"
                  id="password"
                  name="password"
                  ref={password}
                />
              </li>
            </ul>
          </li>
          {inlineView}
          <li>
            <ul>
              <li>
                <input
                  type="submit"
                  value="Login"
                  onClick={() => onClickBtnSubmit()}
                ></input>
              </li>
              <li>
                <input
                  type="button"
                  value="Reset"
                  onClick={() => onClickBtnReset()}
                ></input>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </Loader>
  );
}

export default LoginViewController;
